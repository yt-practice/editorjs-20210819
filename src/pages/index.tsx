import dynamic from 'next/dynamic'

const BlockEditor = dynamic(() => import('../comps/block-editor'), {
	ssr: false,
})

const Home = () => {
	return (
		<div>
			<BlockEditor />
		</div>
	)
}

export default Home
