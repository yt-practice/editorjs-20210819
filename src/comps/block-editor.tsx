import { useEffect, useRef } from 'react'
import EditorJS from '@editorjs/editorjs'

// @ts-expect-error: ignore
import Header from '@editorjs/header'
// @ts-expect-error: ignore
import List from '@editorjs/list'
// @ts-expect-error: ignore
import NestedList from '@editorjs/nested-list'
// @ts-expect-error: ignore
import Delimiter from '@editorjs/delimiter'
// @ts-expect-error: ignore
import Quote from '@editorjs/quote'
// @ts-expect-error: ignore
import Paragraph from '@editorjs/paragraph'
// @ts-expect-error: ignore
import Table from '@editorjs/table'
// @ts-expect-error: ignore
import Checklist from '@editorjs/checklist'

export const BlockEditor = () => {
	const el = useRef<HTMLDivElement>(null)
	useEffect(() => {
		if (!el.current) return
		const editor = new EditorJS({
			holder: el.current,
			onChange: (api, block) => {
				editor.save().then(console.log)
				// console.log(block)
			},
			tools: {
				Header,
				List,
				NestedList,
				Delimiter,
				Quote,
				Paragraph,
				Table,
				Checklist,
			},
			data,
		})
		return () => editor.destroy()
	}, [])
	return (
		<div>
			<div ref={el} />
		</div>
	)
}

export default BlockEditor

const data = {
	time: 1629315228460,
	blocks: [
		{
			id: 'Pzm_4N6T7D',
			type: 'Header',
			data: {
				text: 'ブロックエディタ使ってみるデモ',
				level: 2,
			},
		},
		{
			id: '4Fi1TRd4je',
			type: 'List',
			data: {
				style: 'unordered',
				items: ['hoge', 'fuga', 'piyo'],
			},
		},
		{
			id: 'Q5zcd8Vh_v',
			type: 'paragraph',
			data: {
				text: 'あか<b>さた</b>な',
			},
		},
		{
			id: 'pC5n3MsSLQ',
			type: 'Table',
			data: {
				withHeadings: false,
				content: [
					['id', 'name', 'age'],
					['1', 'tanaka', '12'],
					['2', 'suzuki', '14'],
				],
			},
		},
		{
			id: 'WnM80PaB20',
			type: 'paragraph',
			data: {
				text: 'カラムのサポートが難しそう…？',
			},
		},
		{
			id: '9si6QCGNGf',
			type: 'paragraph',
			data: {
				text: 'aa',
			},
		},
		{
			id: '0QjHTlXh_t',
			type: 'paragraph',
			data: {
				text: 'uuuuuu',
			},
		},
	],
	version: '2.22.2',
}
