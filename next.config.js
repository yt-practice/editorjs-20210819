new Function('return this')().Element = class ElementMock {}
const basePath =
	'development' === process.env.NODE_ENV ? '' : '/editorjs-20210819/'

module.exports = {
	basePath: basePath.replace(/\/$/, ''),
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	trailingSlash: true,
}
